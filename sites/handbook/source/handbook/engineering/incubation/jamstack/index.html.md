---
layout: handbook-page-toc
title: Jamstack Single-Engineer Group
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
  {:toc .hidden-md .hidden-lg}

## Jamstack Single-Engineer Group

The Jamstack SEG is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation/).

This group's mission is to enable Frontend developers to build, deploy and manage externally facing, static websites using Jamstack architecture in a simple, configurable and scalable way.

Frontend engineers should be able to use GitLab as the primary place to manage Jamstack Frontends.

## The Playing Field

Technology supporting Jamstack is a fast-growing, highly innovating field with many startups competing for market shares.

GitLab's solution is built on GitLab Pages and its solid CI/CD pipelines. This allows any client to grow seamlessly from a simple static site to complex, multi-faceted deployments without having to change the provider at any point. Competing in the Jamstack market potentially allows new Developers to have a shallow entry to GitLab's CI/CD world. That is, if we get the User Experience right.

GitLab is not an infrastructure provider. This is an advantage for established clients that may already pay for their own infrastructure. GitLab can easily support a wide range of infrastructure using CI/CD pipelines, so our goal is to make this as simple as possible. It's important that this scalability is conveyed from the very first onboarding steps to build trust with our users, even if most projects will stay small.

## The Vision

Gitlab’s strength is everything-can-be-configured. Gitlab is the Linux of Deployment platforms: Open-source, adaptable, transportable. So Gitlab should be the go-to platform for Jamstack Apps that not only want the whole project lifecycle in one place, but especially those that have non-default needs like onsite-hosting, support for large, complex sites or the need for fine grained control over the intricacies of the deployment.

## How to get there

- [Improving the UX/Frontend of Gitlab Pages](https://gitlab.com/groups/gitlab-org/incubation-engineering/jamstack/-/epics/2). The GitLab Pages backend is of very high quality, but the frontend is very basic and does not convey that level of quality and reliability.
  This is a phase where we catch up on functionality offerings to established alternatives. Although Gitlab’s strengths in the Jamstack space don’t revolve around the single-FE-Developer Persona, we still need to get this experience right to build trust.

- Once the catch-up phase has seen some progress, we can demonstrate GitLab's capabilities by [publishing Blogposts](https://gitlab.com/groups/gitlab-org/incubation-engineering/jamstack/-/epics/7) about:

  - [A How-To on deploying to Pages for each of the most popular FE Frameworks](https://gitlab.com/gitlab-org/incubation-engineering/jamstack/meta/-/issues/20)
  - [Connecting Pages with Fastly and Cloudflare (one post each)](https://gitlab.com/gitlab-org/incubation-engineering/jamstack/meta/-/issues/21)

- [Experiment with a Javascript Runtime Support](https://gitlab.com/groups/gitlab-org/incubation-engineering/jamstack/-/epics/6).
  JS Runtimes are core to SSR (Server-Side Rendering) and the related issues of DPR (Distributed Persistent Rendering) and ISR (Incremental Static Regeneration). These are particularly important for complex sites identified earlier as a key optimization target. In addition, JS Runtimes would also support fullstack frameworks like Remix or serverless functions (see below).
  SSR/DPR/ISR are performance optimization strategies, so ideally the JS runtimes are globally distributed, or even better, deployed on Edge.

- [Experiment with Serverless Functions](https://gitlab.com/groups/gitlab-org/incubation-engineering/jamstack/-/epics/9) again

- Sidetrack: [Git-as-CMS](https://gitlab.com/groups/gitlab-org/incubation-engineering/jamstack/-/epics/10). Git is actually a great tool for Content Management beyond Code: It has collaboration features, version control, access management, interfaces. Editing source files directly is also a great oportunity to have editors and developers collaborate seamlessly. Still, for a successful experience, there needs to be a UI that's designed for the editor persona and is independent of the Git Platform.

  There are already solutions available that do this (i.e. NetlifyCMS, Forestry, Tina), both headless and fullstack, but the options are sparse. The Jamstack SEG should explore whether there is space for an additional framework:

  The idea is to provide a FOSS-, developer-friendly full-stack alternative to Wordpress with static builds for high availability and high performance with a modern stack (e.g. Vue/Vite, or Svelte/Astro).

## Focus Areas

The key focus areas for the Jamstack SEG are:

1. [**Simplicity**](https://gitlab.com/groups/gitlab-org/incubation-engineering/jamstack/-/epics?label_name%5B%5D=Jamstack+Focus%3A%3ASimplicity) - It should be intuitive to use GitLab to deploy a static site. GitLab should be a great place to _just get started_ with the confidence that deployments can be fine-tuned and scaled later.
2. [**Configurability**](https://gitlab.com/groups/gitlab-org/incubation-engineering/jamstack/-/epics?label_name%5B%5D=Jamstack+Focus%3A%3AConfigurability) - With the `.gitlab-ci.yaml` powering the solution, it should be straightforward to support a wide range of deployment strategies.
3. [**Scalability**](https://gitlab.com/groups/gitlab-org/incubation-engineering/jamstack/-/epics?label_name%5B%5D=Jamstack+Focus%3A%3AScaleability) - GitLab should be able to support the entire page's lifecycle. From a quick-and-dirty one-man project to powering the websites of high-traffic global organizations with complex infrastructure needs.

## Backstory

The key to modern Jamstack Frontends are Websites built as Single Page Applications (SPA). Classic Javascript Frameworks like React or Vue.js build the entire HTML DOM on the client side, so to improve performance, they are supplemented by Rendering Engines (eg. Next.js, Nuxt.js) that use node.js to pre-render HTML before they are sent to the client's browser. Frameworks like Eleventy, Gatsby.js or Hugo are both Framework and Rendering Engine.

All frameworks have in common that there are three main approaches as to when the rendering happens:

1. During **Build** time, also known as _Static Site Generation_ (SSG), this is how GitLab Pages works now.
2. When a page is **Requested**, but instead of rendering in the client's browser, the page is pre-rendered by the server, known as _Server Side Rendering_ (SSR)
3. A mix of both, where pages are dynamically rendered once when requested for the first time, then cached, also known as Incremental Static Regeneration (ISR). This is usually supplemented with a step that pre-renders the most popular pages during build.

### Resources

- https://www.smashingmagazine.com/2021/05/evolution-jamstack/

## Issue Link

[https://gitlab.com/gitlab-org/gitlab/-/issues/329597](https://gitlab.com/gitlab-org/gitlab/-/issues/329597)
